package TicketBookingApplication;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;

public class UserDataController {
    private MyFile bookingFile;
    String fs = File.separator;
    private User currentUser;
    private ArrayList<String> arrMovie;
    private ArrayList<String> arrTheaterNo;
    private ArrayList<String> arrSeat;
    private ArrayList<String> arrRound;

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
        usernameLabel.setText(currentUser.getUsername());
        nameLabel.setText(currentUser.getName());
        surnameLabel.setText(currentUser.getSurname());
        emailLabel.setText(currentUser.getEmail());
        read();
    }

    public void read(){
        String theaterNo;
        String round;
        String seat;
        String user;
        String movie;
        try {
            FileReader fileReader = new FileReader(bookingFile.getFilename());
            BufferedReader buffer = new BufferedReader(fileReader);
            String line = buffer.readLine();
            while (line != null) {
                if (!line.isEmpty()) {
                    String read[] = line.split(",");
                    theaterNo = read[0];
                    round = read[1];
                    seat = read[2];
                    user = read[3];
                    movie = read[4];
                    if (user.equals(currentUser.getUsername())) {
                        arrTheaterNo.add(theaterNo);
                        arrMovie.add(movie);
                        arrSeat.add(seat);
                        arrRound.add(round);
                    }
                }
                line = buffer.readLine();
            }
        } catch (IOException e) {
            System.err.println("Error reading file");
        }
        if(!arrMovie.isEmpty()){
            for(int i = 0 ; i < arrMovie.size() ; i++){
                bookedlist.getItems().add("Theater " + arrTheaterNo.get(i) + " | " + arrMovie.get(i) + " | " + arrRound.get(i) + " | " + arrSeat.get(i));
            }
        }

    }

    @FXML
    Button backBtn;
    @FXML public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
        stage.setScene(new Scene(loader.load(),588, 720));
        HomeController homeController = loader.getController();
        if(currentUser != null) {
            homeController.returnWithLogIn(currentUser);
            homeController.setCurrentUser(currentUser);
        }
        stage.show();
    }

    @FXML
    Label usernameLabel, nameLabel, surnameLabel, emailLabel;
    @FXML
    ListView bookedlist;
    @FXML public void initialize() {
        arrMovie = new ArrayList<>();
        arrTheaterNo = new ArrayList<>();
        arrSeat = new ArrayList<>();
        arrRound = new ArrayList<>();
        bookingFile = new MyFile(System.getProperty("user.dir") + fs + "resources" + fs + "Data", "BookingData.csv");
        bookingFile.save();
    }
}
