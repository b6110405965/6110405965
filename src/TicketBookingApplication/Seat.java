package TicketBookingApplication;

public class Seat {
    private String name;
    private double price;
    private boolean isBook;

    public Seat(String name, double price) {
        this.name = name;
        this.price = price;
        this.isBook = false;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public boolean isBook() {
        return isBook;
    }

    public void setBook(boolean book) {
        isBook = book;
    }
}
