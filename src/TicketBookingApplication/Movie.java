package TicketBookingApplication;

import javax.swing.text.html.ImageView;

public class Movie {
    private String name;
    private String length;
    private String sound;
    private String director;
    private String genre;
    private String image;

    public Movie(String name, String length, String sound, String director, String genre, String image){
        this.name = name;
        this.length = length;
        this.sound = sound;
        this.director = director;
        this.genre = genre;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getDirector() {
        return director;
    }

    public String getLength() {
        return length;
    }

    public String getSound() {
        return sound;
    }

    public String getGenre() {
        return genre;
    }

    public String getImage(){
        return image;
    }

}
