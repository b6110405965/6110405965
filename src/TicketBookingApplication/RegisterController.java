package TicketBookingApplication;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class RegisterController implements Initializable {
    private MyFile file;
    String fs = File.separator;
    @FXML
    Button checkBtn, signUpBtn, backBtn;
    @FXML
    TextField usernameText, nameText, surnameText, emailText;
    @FXML
    PasswordField passwordText;
    @FXML
    Label status;
    @FXML
    ImageView statusImg;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        file = new MyFile(System.getProperty("user.dir") + fs + "resources" + fs + "Data", "user.txt");
        signUpBtn.setDisable(true);
        statusImg.setVisible(false);
    }

    @FXML public void handleBackBtnOnAction() throws IOException{
        Stage stage = (Stage) backBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
        stage.setScene(new Scene(loader.load(), 588, 720));
        stage.show();
    }

    @FXML
    public void handleSignUpBtnOnAction(ActionEvent event) throws IOException {
        file.appendWithNewLine("username:" + usernameText.getText());
        file.appendWithNewLine(passwordText.getText());
        file.appendWithNewLine("name:" + nameText.getText());
        file.appendWithNewLine("surname:" + surnameText.getText());
        file.appendWithNewLine("email:" + emailText.getText());
        file.appendWithNewLine("------");
        file.save();
        //return to home
        Stage stage = (Stage) signUpBtn.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
        stage.setScene(new Scene(loader.load(), 588, 720));
        stage.show();
    }

    @FXML
    public void handleCheckBtnOnAction(ActionEvent event) throws IOException {
        int check = 0;
        try {
            FileReader fileReader = new FileReader(file.getFilename());
            BufferedReader buffer = new BufferedReader(fileReader);
            String line = buffer.readLine();
            while (line != null) {
                if (line.contains("username:")) {
                    String[] username = line.split("username:");
                    if (username[1].equals(usernameText.getText())) {
                        check++;
                        break;
                    }
                }
                line = buffer.readLine();
            }
        } catch (IOException e) {
            System.err.println("Error reading file");
        }
        if(check == 0){
            signUpBtn.setDisable(false);
            status.setText(" ");
            statusImg.setVisible(true);
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("This username has been taken.");
            alert.setContentText("Please try another.");
            alert.showAndWait();

        }
    }
}

