package TicketBookingApplication;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class HomeController {
    @FXML Button registerBtn;
    private MyFile userFile;
    private User currentUser;
    Theater theater1 = new Theater("1","4K");
    Theater theater2 = new Theater("2","Normal");
    Theater theater3 = new Theater("3","4K");
    Theater theater4 = new Theater("4","3D");

    Movie hereditary = new Movie("Hereditary","2hrs 7min","English","Ari Aster","Drama, Horror, Mystery" ,"hereditary.jpg");
    Movie itfollows = new Movie("It Follows" , "1hrs 40min", "English", "David Robert Mitchell","Horror, Mystery, Thriller", "itfollows.jpg");
    Movie parasite = new Movie("Parasite", "2hrs 12min", "Korean", "Joon-ho Bong", "Comedy, Drama, Thriller", "parasite.jpg");
    Movie midsomma = new Movie("Midsomma" , "2hrs 27min", "English", "Ari Aster","Drama, Horror, Mystery", "midsomma.jpg");
    Movie ready = new Movie("Ready or Not", "1hrs 35min", "English", "Matt Bettinelli-Olpin, Tyler Gillett", "Comedy, Horror, Mystery", "ready.jpg");
    Movie it = new Movie("It Chapter2","2hrs 49min","English","Andy Muschietti","Drama, Fantasy, Horror", "it.jpg");



    @FXML Button hereditaryBtn, itfollowsBtn, parasiteBtn, midsommaBtn, readyBtn, itBtn, memberBtn;

    @FXML public void initialize(){
        memberBtn.setVisible(false);
        signOutBtn.setVisible(false);
        currentUser = null;
        String fs = File.separator;
        userFile = new MyFile(System.getProperty("user.dir") + fs + "resources" + fs + "Data", "user.txt");
        userFile.save();
    }


    @FXML public void handleHereditaryOnAction(ActionEvent event) throws IOException {
        Button bhereditary = (Button) event.getSource();
        Stage stage = (Stage) bhereditary.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater1Preview.fxml"));
        stage.setScene(new Scene(loader.load(),588, 707));
        Theater1PreviewController theater1PreviewController = loader.getController();
        theater1PreviewController.setTheater1(theater1);
        theater1PreviewController.setMovie(hereditary);
        theater1PreviewController.setCurrentUser(currentUser);
        stage.show();
    }

    @FXML public void handleItFollowsOnAction(ActionEvent event) throws IOException {
        Button bitfollows = (Button) event.getSource();
        Stage stage = (Stage) bitfollows.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater2Preview.fxml"));
        stage.setScene(new Scene(loader.load(),588, 707));
        Theater2PreviewController theater2PreviewController = loader.getController();
        theater2PreviewController.setTheater2(theater2);
        theater2PreviewController.setMovie(itfollows);
        theater2PreviewController.setMovie(parasite);
        theater2PreviewController.setCurrentUser(currentUser);
        stage.show();
    }
    @FXML public void handleParasiteOnAction(ActionEvent event) throws IOException {
        Button bparasite = (Button) event.getSource();
        Stage stage = (Stage) bparasite.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater2Preview.fxml"));
        stage.setScene(new Scene(loader.load(),588, 707));
        Theater2PreviewController theater2PreviewController = loader.getController();
        theater2PreviewController.setTheater2(theater2);
        theater2PreviewController.setMovie(itfollows);
        theater2PreviewController.setMovie(parasite);
        theater2PreviewController.setCurrentUser(currentUser);
        stage.show();
    }

    @FXML public void handleMidsommaOnAction(ActionEvent event) throws IOException {
        Button bmidsomma = (Button) event.getSource();
        Stage stage = (Stage) bmidsomma.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater3Preview.fxml"));
        stage.setScene(new Scene(loader.load(),591, 707));
        Theater3PreviewController theater3PreviewController = loader.getController();
        theater3PreviewController.setTheater3(theater3);
        theater3PreviewController.setMovie(midsomma);
        theater3PreviewController.setMovie(ready);
        theater3PreviewController.setCurrentUser(currentUser);
        stage.show();
    }
    @FXML public void handleReadyOnAction(ActionEvent event) throws IOException {
        Button bready = (Button) event.getSource();
        Stage stage = (Stage) bready.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater3Preview.fxml"));
        stage.setScene(new Scene(loader.load(),591, 707));
        Theater3PreviewController theater3PreviewController = loader.getController();
        theater3PreviewController.setTheater3(theater3);
        theater3PreviewController.setMovie(midsomma);
        theater3PreviewController.setMovie(ready);
        theater3PreviewController.setCurrentUser(currentUser);
        stage.show();
    }

    @FXML public void handleItOnAction(ActionEvent event) throws IOException {
        Button bit = (Button) event.getSource();
        Stage stage = (Stage) bit.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater4Preview.fxml"));
        stage.setScene(new Scene(loader.load(),588, 707));
        Theater4PreviewController theater4PreviewController = loader.getController();
        theater4PreviewController.setTheater4(theater4);
        theater4PreviewController.setMovie(it);
        theater4PreviewController.setCurrentUser(currentUser);
        stage.show();
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @FXML Button signInBtn, signOutBtn;
    @FXML TextField usernameField;
    @FXML PasswordField passwordField;
    @FXML Label welcomeLabel;
    String fs = File.separator;
    @FXML public void handleSignInBtnOnAction(ActionEvent event) throws Exception {
        int check = 0;
        String username = "", password = "", name = "", surname = "", email = "";
        try{
            FileReader fileReader = new FileReader(userFile.getFilename());
            BufferedReader buffer = new BufferedReader(fileReader);
            String line = buffer.readLine();
            while (line != null) {
                if(line.contains("username:")) {
                    String[] username1 = line.split("username:");
                    username = username1[1];
                    if (username1[1].equals(usernameField.getText())) {
                        password = buffer.readLine();
                        if (password.equals(passwordField.getText())) {
                            welcomeLabel.setText("Welcome " + usernameField.getText());
                            String line1 = buffer.readLine();
                            String name1[] = line1.split("name:");
                            name = name1[1];
                            String line2 = buffer.readLine();
                            String surname1[] = line2.split("surname:");
                            surname = surname1[1];
                            String line3 = buffer.readLine();
                            String email1[] = line3.split("email:");
                            email = email1[1];
                            usernameField.setDisable(true);
                            passwordField.setDisable(true);
                            signInBtn.setVisible(false);
                            registerBtn.setVisible(false);
                            signOutBtn.setVisible(true);
                            memberBtn.setVisible(true);
                            check = 1;
                            break;
                        } else {
                            //welcomeLabel.setText("Wrong Password");
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error");
                            alert.setHeaderText("Wrong Password");
                            alert.setContentText("Please enter password again.");
                            alert.showAndWait();
                            username = " ";
                            check = 1;
                        }
                    }
                }
                line = buffer.readLine();
            }
            if (check == 0) {
                //welcomeLabel.setText("Username doesn't exist");
                username = " ";
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Username doesn't exist");
                alert.setContentText("Please enter username and password again. \nIf You don't have any account please sign up.");
                alert.showAndWait();
            }
            if(username != " ") {
                currentUser = new User(username, password, name, surname, email);
            }
            else if(username == " "){
                currentUser = null;
            }
        }
        catch (IOException e){
            System.err.println("Error reading file");
        }
    }

    @FXML public void handleMemberBtnOnAction(ActionEvent event) throws IOException{
        Button b = (Button)event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("UserDataPage.fxml"));
        stage.setScene(new Scene(loader.load(),512, 627));
        UserDataController userDataController = loader.getController();
        userDataController.setCurrentUser(currentUser);
        stage.show();
    }

    @FXML public void handleSignUpBtnOnAction(ActionEvent event) throws Exception{
        Button b = (Button)event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Register.fxml"));
        stage.setScene(new Scene(loader.load(),600, 400));
        stage.show();
    }


    @FXML public void handleSignOutBtnOnAction(ActionEvent event) throws Exception{
        currentUser = null;
        usernameField.setDisable(false);
        usernameField.clear();
        passwordField.setDisable(false);
        passwordField.clear();
        signInBtn.setVisible(true);
        registerBtn.setVisible(true);
        signOutBtn.setVisible(false);
        memberBtn.setVisible(false);
        welcomeLabel.setText("");
    }

    @FXML public void returnWithLogIn(User currentUser){
        usernameField.setText(currentUser.getUsername());
        usernameField.setDisable(true);
        passwordField.setText(currentUser.getPassword());
        passwordField.setDisable(true);
        welcomeLabel.setText("Welcome " + currentUser.getUsername());
        signInBtn.setVisible(false);
        registerBtn.setVisible(false);
        memberBtn.setVisible(true);
        signOutBtn.setVisible(true);
    }

    @FXML Button credit;
    @FXML public void handleCreditBtnOnAction(ActionEvent event) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Credit.fxml"));
        Parent root1 = (Parent) loader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.setTitle("Credit");
        stage.show();
    }
}
