package TicketBookingApplication;

import java.util.ArrayList;

public class Theater {
    private String theaterNumber;
    private String system;
    private ArrayList<Movie> movie = new ArrayList<Movie>();
    private ArrayList<Seat> seats = new ArrayList<Seat>();
    private ArrayList<String> round = new ArrayList<String>();

    public Theater(String theaterNumber, String system){
        this.system = system;
        this.theaterNumber = theaterNumber;
    }

    public Theater(String theaterNumber, String system, ArrayList<String> round){
        this.system = system;
        this.theaterNumber = theaterNumber;
        this.round = round;
    }

    public ArrayList<Movie> getMovie() {
        return movie;
    }

    public void addMovie(Movie movie){
        this.movie.add(movie);
    }

    public String getSystem() {
        return system;
    }

    public String getTheaterNumber() {
        return theaterNumber;
    }

    public void setSeats(ArrayList<Seat> seats) {
        this.seats = seats;
    }
}
