package TicketBookingApplication;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class Theater3Controller {
    private Theater theater;
    private String round;
    private Movie movie;
    private ArrayList<String> selectedSeat;
    private double price;
    private User currentUser;
    private ArrayList<String> bookedSeat;

    public void setTheater(Theater theater , String round){
        this.theater = theater;
        this.round = round;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    @FXML
    Label name, theaterNo, sound, roundLabel, bookedLabel;

    @FXML
    Button backBtn, homeBtn, bookBtn;

    @FXML
    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                name.setText(movie.getName());
                theaterNo.setText("Theater " + theater.getTheaterNumber());
                roundLabel.setText(round);
                sound.setText(movie.getSound());
                selectedSeat = new ArrayList<>();
                price = 0.0;
                bookFile = new MyFile(System.getProperty("user.dir") + fs + "resources" + fs + "Data", "BookingData.csv");
            }
        });
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void setBookedSeat(ArrayList<String> bookedSeat) {
        this.bookedSeat = bookedSeat;
        if (!bookedSeat.isEmpty()) {
            if (bookedSeat.contains("A1")) {
                a1.setDisable(true);
            }
            if (bookedSeat.contains("A2")) {
                a2.setDisable(true);
            }
            if (bookedSeat.contains("A3")) {
                a3.setDisable(true);
            }
            if (bookedSeat.contains("A4")) {
                a4.setDisable(true);
            }
            if (bookedSeat.contains("A5")) {
                a5.setDisable(true);
            }
            if (bookedSeat.contains("A6")) {
                a6.setDisable(true);
            }
            if (bookedSeat.contains("A7")) {
                a7.setDisable(true);
            }
            if (bookedSeat.contains("B1")) {
                b1.setDisable(true);
            }
            if (bookedSeat.contains("B2")) {
                b2.setDisable(true);
            }
            if (bookedSeat.contains("B3")) {
                b3.setDisable(true);
            }
            if (bookedSeat.contains("B4")) {
                b4.setDisable(true);
            }
            if (bookedSeat.contains("B5")) {
                b5.setDisable(true);
            }
            if (bookedSeat.contains("B6")) {
                b6.setDisable(true);
            }
            if (bookedSeat.contains("B7")) {
                b7.setDisable(true);
            }
            if (bookedSeat.contains("C1")) {
                c1.setDisable(true);
            }
            if (bookedSeat.contains("C2")) {
                c2.setDisable(true);
            }
            if (bookedSeat.contains("C3")) {
                c3.setDisable(true);
            }
            if (bookedSeat.contains("C4")) {
                c4.setDisable(true);
            }
            if (bookedSeat.contains("C5")) {
                c5.setDisable(true);
            }
            if (bookedSeat.contains("C6")) {
                c6.setDisable(true);
            }
            if (bookedSeat.contains("C7")) {
                c7.setDisable(true);
            }
            if (bookedSeat.contains("D1")) {
                d1.setDisable(true);
            }
            if (bookedSeat.contains("D2")) {
                d2.setDisable(true);
            }
            if (bookedSeat.contains("D3")) {
                d3.setDisable(true);
            }
            if (bookedSeat.contains("D4")) {
                d4.setDisable(true);
            }
            if (bookedSeat.contains("D5")) {
                d5.setDisable(true);
            }
            if (bookedSeat.contains("D6")) {
                d6.setDisable(true);
            }
            if (bookedSeat.contains("D7")) {
                d7.setDisable(true);
            }
        }
    }

    @FXML public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater3Preview.fxml"));
        stage.setScene(new Scene(loader.load(),588, 707));
        Theater3PreviewController theater3PreviewController = loader.getController();
        theater3PreviewController.setTheater3(theater);
        theater3PreviewController.setCurrentUser(currentUser);
        stage.show();
    }

    @FXML public void handleHomeBtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
        stage.setScene(new Scene(loader.load(),588, 720));
        if(currentUser != null) {
            HomeController homeController = loader.getController();
            homeController.returnWithLogIn(currentUser);
            homeController.setCurrentUser(currentUser);
        }
        stage.show();
    }

    @FXML Label selectedSeatLabel, priceLabel;
    @FXML Button a1, a2, a3, a4, a5, a6, a7;
    @FXML Button b1, b2, b3, b4, b5, b6, b7;
    @FXML Button c1, c2, c3, c4, c5, c6, c7;
    @FXML Button d1, d2, d3, d4, d5, d6, d7;
    Image imageSelect = new Image("image/select.png");
    @FXML public void handleA1OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("A1")){
            a1.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("A1");
            price -= 270;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            a1.setPrefSize(50, 45);
            a1.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("A1");
            price += 270;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleA2OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("A2")){
            a2.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("A2");
            price -= 270;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            a2.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("A2");
            price += 270;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleA3OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("A3")){
            a3.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("A3");
            price -= 270;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            a3.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("A3");
            price += 270;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleA4OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("A4")){
            a4.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("A4");
            price -= 270;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            a4.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("A4");
            price += 270;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleA5OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("A5")){
            a5.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("A5");
            price -= 270;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            a5.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("A5");
            price += 270;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleA6OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("A6")){
            a6.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("A6");
            price -= 270;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            a6.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("A6");
            price += 270;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleA7OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("A7")){
            a7.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("A7");
            price -= 270;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            a7.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("A7");
            price += 270;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleB1OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("B1")){
            b1.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("B1");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            b1.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("B1");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleB2OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("B2")){
            b2.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("B2");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            b2.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("B2");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleB3OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("B3")){
            b3.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("B3");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            b3.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("B3");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleB4OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("B4")){
            b4.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("B4");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            b4.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("B4");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleB5OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("B5")){
            b5.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("B5");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            b5.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("B5");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleB6OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("B6")){
            b6.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("B6");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            b6.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("B6");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleB7OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("B7")){
            b7.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("B7");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            b7.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("B7");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleC1OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("C1")){
            c1.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("C1");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            c1.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("C1");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleC2OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("C2")){
            c2.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("C2");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            c2.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("C2");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleC3OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("C3")){
            c3.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("C3");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            c3.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("C3");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleC4OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("C4")){
            c4.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("C4");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            c4.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("C4");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleC5OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("C5")){
            c5.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("C5");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            c5.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("C5");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleC6OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("C6")){
            c6.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("C6");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            c6.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("C6");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleC7OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("C7")){
            c7.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("C7");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            c7.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("C7");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleD1OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("D1")){
            d1.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("D1");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            d1.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("D1");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleD2OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("D2")){
            d2.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("D2");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            d2.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("D2");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleD3OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("D3")){
            d3.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("D3");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            d3.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("D3");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleD4OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("D4")){
            d4.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("D4");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            d4.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("D4");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleD5OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("D5")){
            d5.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("D5");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            d5.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("D5");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleD6OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("D6")){
            d6.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("D6");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            d6.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("D6");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }
    @FXML public void handleD7OnAction(ActionEvent event) throws Exception{
        if(selectedSeat.contains("D7")){
            d7.setGraphic(new ImageView(imageUnselect));
            selectedSeat.remove("D7");
            price -= 240;
            if(selectedSeat.isEmpty()){
                selectedSeatLabel.setText(" ");
                priceLabel.setText(" ");
            }
            else {
                selectedSeatLabel.setText(String.valueOf(selectedSeat));
                priceLabel.setText(String.valueOf(price) + " Bath");
            }
        }
        else {
            d7.setGraphic(new ImageView(imageSelect));
            selectedSeat.add("D7");
            price += 240;
            selectedSeatLabel.setText(String.valueOf(selectedSeat));
            priceLabel.setText(String.valueOf(price) + " Bath");
        }
    }

    Image imageUnselect = new Image("image/unselect.png");
    @FXML Button unselectBtn;
    @FXML public void handleUnselectBtnOnAction(ActionEvent event) throws Exception{
        price = 0;
        selectedSeat.clear();
        selectedSeatLabel.setText(" ");
        priceLabel.setText(" ");
        a1.setGraphic(new ImageView(imageUnselect));
        a2.setGraphic(new ImageView(imageUnselect));
        a3.setGraphic(new ImageView(imageUnselect));
        a4.setGraphic(new ImageView(imageUnselect));
        a5.setGraphic(new ImageView(imageUnselect));
        a6.setGraphic(new ImageView(imageUnselect));
        a7.setGraphic(new ImageView(imageUnselect));
        b1.setGraphic(new ImageView(imageUnselect));
        b2.setGraphic(new ImageView(imageUnselect));
        b3.setGraphic(new ImageView(imageUnselect));
        b4.setGraphic(new ImageView(imageUnselect));
        b5.setGraphic(new ImageView(imageUnselect));
        b6.setGraphic(new ImageView(imageUnselect));
        b7.setGraphic(new ImageView(imageUnselect));
        c1.setGraphic(new ImageView(imageUnselect));
        c2.setGraphic(new ImageView(imageUnselect));
        c3.setGraphic(new ImageView(imageUnselect));
        c4.setGraphic(new ImageView(imageUnselect));
        c5.setGraphic(new ImageView(imageUnselect));
        c6.setGraphic(new ImageView(imageUnselect));
        c7.setGraphic(new ImageView(imageUnselect));
        d1.setGraphic(new ImageView(imageUnselect));
        d2.setGraphic(new ImageView(imageUnselect));
        d3.setGraphic(new ImageView(imageUnselect));
        d4.setGraphic(new ImageView(imageUnselect));
        d5.setGraphic(new ImageView(imageUnselect));
        d6.setGraphic(new ImageView(imageUnselect));
        d7.setGraphic(new ImageView(imageUnselect));
    }


    private MyFile bookFile;
    String fs = File.separator;
    @FXML public void handleBookBtnOnAction(ActionEvent event) throws Exception{
        if(currentUser == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Can not book");
            alert.setContentText("Please sign in before booking.");
            alert.showAndWait();
            Stage stage = (Stage) bookBtn.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
            stage.setScene(new Scene(loader.load(),588, 720));
            if(currentUser != null) {
                HomeController homeController = loader.getController();
                homeController.returnWithLogIn(currentUser);
                homeController.setCurrentUser(currentUser);
            }
            stage.show();

        }
        else {
            if(selectedSeat.isEmpty()){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("No Selected Seat");
                alert.setContentText("Please select seat to book.");
                alert.showAndWait();
            }
            else{
                Alert alert1 = new Alert(Alert.AlertType.CONFIRMATION);
                alert1.setTitle("Confirmation Booking");
                alert1.setHeaderText("Please check your booking");
                alert1.setContentText("Seat : " + selectedSeat + "\nPrice : " + price);
                Optional<ButtonType> result = alert1.showAndWait();
                if (result.get() == ButtonType.OK){
                    for(int i = 0 ; i < selectedSeat.size() ; i++) {
                        bookFile.appendWithNewLine(theater.getTheaterNumber() + "," + round + "," + selectedSeat.get(i) + "," + currentUser.getUsername() + "," + name.getText());
                    }
                    bookFile.save();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Success");
                    alert.setHeaderText("Booking Success");
                    alert.setContentText("Your booking is success. \nPlease check in member part.");
                    alert.showAndWait();
                    Stage stage = (Stage) bookBtn.getScene().getWindow();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
                    stage.setScene(new Scene(loader.load(),588, 720));
                    if(currentUser != null) {
                        HomeController homeController = loader.getController();
                        homeController.returnWithLogIn(currentUser);
                        homeController.setCurrentUser(currentUser);
                    }
                    stage.show();
                } else {
                    alert1.close();
                }

            }
        }

    }
}

