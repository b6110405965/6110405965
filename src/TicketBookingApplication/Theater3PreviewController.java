package TicketBookingApplication;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Theater3PreviewController {
    private Theater theater3;
    private User currentUser;
    private MyFile bookingFile;
    String fs = File.separator;

    @FXML
    Label m2name , m2director, m2genre, m2length, m1name , m1director, m1genre, m1length;
    @FXML
    Button backBtn;
    @FXML
    Button round1, round2, round3, round4;

    @FXML public void initialize() {

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                m1name.setText(theater3.getMovie().get(0).getName());
                m1director.setText(theater3.getMovie().get(0).getDirector());
                m1genre.setText(theater3.getMovie().get(0).getGenre());
                m1length.setText(theater3.getMovie().get(0).getLength());
                m2name.setText(theater3.getMovie().get(1).getName());
                m2director.setText(theater3.getMovie().get(1).getDirector());
                m2genre.setText(theater3.getMovie().get(1).getGenre());
                m2length.setText(theater3.getMovie().get(1).getLength());
                bookingFile = new MyFile(System.getProperty("user.dir") + fs + "resources" + fs + "Data", "BookingData.csv");
                bookingFile.save();
            }
        });
    }
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    @FXML public void handleBackBtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
        stage.setScene(new Scene(loader.load(),588, 720));
        HomeController homeController = loader.getController();
        if(currentUser != null) {
            homeController.returnWithLogIn(currentUser);
            homeController.setCurrentUser(currentUser);
        }
        stage.show();
    }


    public void setTheater3(Theater theater3) {
        this.theater3 = theater3;
    }
    public void setMovie(Movie movie){
        theater3.addMovie(movie);
    }

    @FXML public void handleRound1BtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater3.fxml"));
        stage.setScene(new Scene(loader.load(),800, 670));
        Theater3Controller theater3ControllerRound1 = loader.getController();
        theater3ControllerRound1.setTheater(theater3, round1.getText());
        theater3ControllerRound1.setMovie(theater3.getMovie().get(0));
        theater3ControllerRound1.setCurrentUser(currentUser);
        ArrayList<String> bookedSeatRound1 = new ArrayList<>();
        try{
            FileReader fileReader = new FileReader(bookingFile.getFilename());
            BufferedReader buffer = new BufferedReader(fileReader);
            String line = buffer.readLine();
            String theaterNo, round, seat, user;
            while (line != null) {
                if(!line.isEmpty()) {
                    String read[] = line.split(",");
                    theaterNo = read[0];
                    round = read[1];
                    seat = read[2];
                    user = read[3];
                    if (theaterNo.equals("3") && round.equals(round1.getText())) {
                        bookedSeatRound1.add(seat);
                    }
                }
                line = buffer.readLine();
            }} catch (IOException e){
            System.err.println("Error reading file");
        }
        theater3ControllerRound1.setBookedSeat(bookedSeatRound1);
        stage.show();
    }
    @FXML public void handleRound2BtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater3.fxml"));
        stage.setScene(new Scene(loader.load(),800, 670));
        Theater3Controller theater3ControllerRound2 = loader.getController();
        theater3ControllerRound2.setTheater(theater3, round2.getText());
        theater3ControllerRound2.setMovie(theater3.getMovie().get(0));
        theater3ControllerRound2.setCurrentUser(currentUser);
        ArrayList<String> bookedSeatRound2 = new ArrayList<>();
        try{
            FileReader fileReader = new FileReader(bookingFile.getFilename());
            BufferedReader buffer = new BufferedReader(fileReader);
            String line = buffer.readLine();
            String theaterNo, round, seat, user;
            while (line != null) {
                if(!line.isEmpty()) {
                    String read[] = line.split(",");
                    theaterNo = read[0];
                    round = read[1];
                    seat = read[2];
                    user = read[3];
                    if (theaterNo.equals("3") && round.equals(round2.getText())) {
                        bookedSeatRound2.add(seat);
                    }
                }
                line = buffer.readLine();
            }} catch (IOException e){
            System.err.println("Error reading file");
        }
        theater3ControllerRound2.setBookedSeat(bookedSeatRound2);
        stage.show();
    }

    @FXML public void handleRound3BtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater3.fxml"));
        stage.setScene(new Scene(loader.load(),800, 670));
        Theater3Controller theater3ControllerRound3 = loader.getController();
        theater3ControllerRound3.setTheater(theater3, round3.getText());
        theater3ControllerRound3.setMovie(theater3.getMovie().get(1));
        theater3ControllerRound3.setCurrentUser(currentUser);
        ArrayList<String> bookedSeatRound3 = new ArrayList<>();
        try{
            FileReader fileReader = new FileReader(bookingFile.getFilename());
            BufferedReader buffer = new BufferedReader(fileReader);
            String line = buffer.readLine();
            String theaterNo, round, seat, user;
            while (line != null) {
                if(!line.isEmpty()) {
                    String read[] = line.split(",");
                    theaterNo = read[0];
                    round = read[1];
                    seat = read[2];
                    user = read[3];
                    if (theaterNo.equals("3") && round.equals(round3.getText())) {
                        bookedSeatRound3.add(seat);
                    }
                }
                line = buffer.readLine();
            }} catch (IOException e){
            System.err.println("Error reading file");
        }
        theater3ControllerRound3.setBookedSeat(bookedSeatRound3);
        stage.show();
    }

    @FXML public void handleRound4BtnOnAction(ActionEvent event) throws IOException {
        Button b = (Button) event.getSource();
        Stage stage = (Stage) b.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater3.fxml"));
        stage.setScene(new Scene(loader.load(),800, 670));
        Theater3Controller theater3ControllerRound4 = loader.getController();
        theater3ControllerRound4.setTheater(theater3, round4.getText());
        theater3ControllerRound4.setMovie(theater3.getMovie().get(1));
        theater3ControllerRound4.setCurrentUser(currentUser);
        ArrayList<String> bookedSeatRound4 = new ArrayList<>();
        try{
            FileReader fileReader = new FileReader(bookingFile.getFilename());
            BufferedReader buffer = new BufferedReader(fileReader);
            String line = buffer.readLine();
            String theaterNo, round, seat, user;
            while (line != null) {
                if(!line.isEmpty()) {
                    String read[] = line.split(",");
                    theaterNo = read[0];
                    round = read[1];
                    seat = read[2];
                    user = read[3];
                    if (theaterNo.equals("3") && round.equals(round4.getText())) {
                        bookedSeatRound4.add(seat);
                    }
                }
                line = buffer.readLine();
            }} catch (IOException e){
            System.err.println("Error reading file");
        }
        theater3ControllerRound4.setBookedSeat(bookedSeatRound4);
        stage.show();
    }
}

